#!/usr/bin/python3

"""Find the Channel ID for a given channel URL."""
from bs4 import BeautifulSoup
from sys import argv
import urllib.request

channelPage = urllib.request.urlopen(argv[1]).read()
soup = BeautifulSoup(channelPage, 'lxml')
channelid = soup.find('meta', attrs={'itemprop':'channelId'})['content']
print(channelid)
