import os
from flask_wtf import FlaskForm
from wtforms import SelectField, StringField, SubmitField
from wtforms.validators import DataRequired


def findFolders(location):
	"""List the folders in location."""
	currentDir = os.getcwd()
	os.chdir(os.path.abspath(location))
	for item in os.listdir():
		if os.path.isfile(item):
			continue
		else:
			yield item
	os.chdir(currentDir)


def betterNames(themeList):
	outList = []
	for theme in themeList:
		for char in theme:
			if char.isupper():
				outList.append(theme.replace(char, ' ' + char.lower()))
	return outList


def themeChoiceGenerator():
	themes = list(findFolders('app/static/css/themes'))
	displayNames = betterNames(themes)
	outputList = []
	for i in range(len(themes)):
		outputList.append((themes[i], displayNames[i]))
	return outputList


class themeSelectForm(FlaskForm):
	theme = SelectField('Theme', choices=themeChoiceGenerator())
	submit = SubmitField('Go!')


class changeSubscriptions(FlaskForm):
	channelid = StringField('Channel ID', validators=[DataRequired()])
	add = SubmitField('Add')
	remove = SubmitField('Remove')
