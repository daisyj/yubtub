"""Parse the subscriptons.json file provided by Google."""

import json
from os import path


def channelSortPattern(e):
	"""Get the channel name."""
	return e['name'].lower()


def deduplicateSubscriptions(subList):
	"""Remove duplicates from a subscription list."""
	return [i for n, i in enumerate(subList) if i not in subList[n + 1:]]


def parseJSON(filename):
	"""Parse the subscriptions.json file."""
	# open filename, turn it into a json object, and then close the file
	subscriptionsFile = open(path.abspath(filename), 'rt')
	jsonSubs = json.load(subscriptionsFile)
	subscriptionsFile.close()
	return jsonSubs


def simplifyList(jsonSubs):
	"""Create somplified list of subscriptions from Google's JSON."""
	# create simplified list of subscriptions from json object
	# (contains just the name, channelId, and avatar at 64px)
	subList = []
	for channel in jsonSubs:
		subList.append({
			'name': channel['snippet']['title'],
			'channelid': channel['snippet']['resourceId']['channelId'],
			'avatar': channel['snippet']['thumbnails']['default']['url'][:-24] + '64',
			'description': channel['snippet']['description'],
		})
	return subList


def sortSubs(subList):
	"""Sort simplified subscription list by channel name."""
	subList.sort(key=channelSortPattern)
	return subList


def importLists(subsComplicated=None, subsSimple=None):
	"""Import subscription lists from supplied file(s)."""
	complicatedList = []
	# read in and simplify Google's json file
	if path.isfile(subsComplicated or '../personalData/subscriptions.json'):
		complicatedList = parseJSON(subsComplicated)
		complicatedList = simplifyList(complicatedList)

	simpleList = []
	# read in subSimple
	if path.isfile(subsSimple):
		simpleList = parseJSON(subsSimple)

	# combine complicatedList and simpleList into subsList and remove duplicates
	subsList = deduplicateSubscriptions(complicatedList + simpleList)

	# sort subsList
	subsList = sortSubs(subsList)

	# write subsListJSON to subsSimple
	simpleFile = open(path.abspath(subsSimple) or '../personalData/subscriptionsSimplified.json', 'wt')
	json.dump(subsList, simpleFile, separators=(',', ':'))
	simpleFile.close()
	return subsList
