"""Manage subscriptions."""
from bs4 import BeautifulSoup
from json import dump
import re
import urllib.request
from .feedParser import getFeed
from .subsParser import importLists, sortSubs


def addChannel(channelid):
	"""Add a channel to the list."""
	channelPage, channelid = getChannelPage(channelid)
	channelFeed = getFeed(channelid)
	subsList = importLists(subsSimple='app/personalData/subscriptionsSimplified.json')
	try:
		subsList.append({
			"name": channelFeed.author.find('name').string,
			"channelid": channelid,
			"avatar": re.findall('^[^=]*', channelPage.find('link', rel='image_src')['href'])[0] + '=s64',
			"description": channelPage.find('meta', attrs={'property':'og:description'})['content']
		})
	except:
		subsList.append({
			"name": channelFeed.author.find('name').string,
			"channelid": channelid,
			"avatar": re.findall('^[^=]*', channelPage.find('link', rel='image_src')['href'])[0] + '=s64',
			"description": ''
		})
	subsList = sortSubs(subsList)
	simpleFile = open('app/personalData/subscriptionsSimplified.json', 'wt')
	dump(subsList, simpleFile, separators=(',', ':'))
	simpleFile.close()


def removeChannel(channelid):
	"""Remove a channel from the list."""
	subsList = importLists(subsSimple='app/personalData/subscriptionsSimplified.json')
	for i in range(len(subsList)):
		print(subsList[i])
		if subsList[i].get('channelid') == channelid:
			del subsList[i]
			break
	simpleFile = open('app/personalData/subscriptionsSimplified.json', 'wt')
	dump(subsList, simpleFile, separators=(',', ':'))
	simpleFile.close()


def getChannelPage(channelid):
	"""Get the page for YouTube channel <channelid> as a BeautifulSoup object."""
	if channelid.find('https://www.youtube.com/') == -1:
		feed = urllib.request.urlopen('https://www.youtube.com/channel/'+channelid+'/about').read()
	else:
		feed = urllib.request.urlopen(channelid).read()
	soup = BeautifulSoup(feed, 'lxml')
	channelid = soup.find('meta', attrs={'itemprop':'channelId'})['content']
	return soup, channelid
