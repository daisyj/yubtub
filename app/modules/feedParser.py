"""Parse YouTube channel XML RSS feeds."""


from bs4 import BeautifulSoup
from datetime import datetime
import urllib.request
import threading


def getFeed(channelid):
	"""Get the XML RSS feed for YouTube channel <channelid>."""
	feed = urllib.request.urlopen('https://www.youtube.com/feeds/videos.xml?channel_id=' + channelid).read()
	soup = BeautifulSoup(feed, 'lxml')
	return soup


def getVideos(channelId, outFeed, num):
	"""Get the <num> most recent videos from <channelId>, stored in <outFeed>."""
	feed = getFeed(channelId)
	# print(channelId)
	videos = feed('entry', limit=num)
	for video in videos:
		outFeed.append({
			'channel': video.find('name').string,
			'channelLink': video.author.uri.string,
			'videoTitle': video.title.string,
			'videoLink': video.link['href'],
			'videoThumbnail': 'https://i.ytimg.com/vi/' + video.find('yt:videoid').string + '/mqdefault.jpg',
			'videoDescription': video.find('media:description').string,
			'publishDisp': datetime.strptime(video.published.string, '%Y-%m-%dT%H:%M:%S+00:00').strftime('%b %d, %Y at %I:%M %p'),
			'publishSort': datetime.strptime(video.published.string, '%Y-%m-%dT%H:%M:%S+00:00').strftime('%Y%m%d%H%M%S')
		})


def getSorted(e):
	"""Provide sorting key."""
	return e['publishSort']


def populateFeed(subList, num):
	"""Populate <feed> to contain the <num> latest videos from the channels contained in <subList>."""
	feed = []
	threads = []
	for i in range(len(subList)):
		threads.append(threading.Thread(target=getVideos, args=(subList[i]['channelid'], feed, num)))
	for i in threads:
		i.start()
	for i in threads:
		i.join()
	feed.sort(reverse=True, key=getSorted)
	return feed
