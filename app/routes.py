"""Where the action happens."""

from app import app
from app.forms import changeSubscriptions, themeSelectForm
from flask import redirect, render_template
from .modules.feedParser import populateFeed
from .modules.subsManager import addChannel, removeChannel
from .modules.subsParser import importLists


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def subFeed():
	"""List recent videos from subscriptions."""
	subList = importLists(subsComplicated='app/personalData/subscriptions.json', subsSimple='app/personalData/subscriptionsSimplified.json')
	vidFeed = populateFeed(subList, (app.config['VID_COUNT'] or 3))
	themeForm = themeSelectForm()
	if themeForm.submit.data:
		return redirect('/changeTheme/' + themeForm.theme.data)
	return render_template('subFeed.html', title='Yubtub homepage',
		contents=vidFeed, config=app.config, themeForm=themeForm)


@app.route('/subList', methods=['GET', 'POST'])
def subList():
	"""List people subscribed to."""
	themeForm = themeSelectForm()
	channelManager = changeSubscriptions()
	if themeForm.submit.data:
		return redirect('/changeTheme/' + themeForm.theme.data)
	if channelManager.validate_on_submit():
		if channelManager.add.data:
			addChannel(channelManager.channelid.data)
		if channelManager.remove.data:
			removeChannel(channelManager.channelid.data)
	subList = importLists(subsComplicated='app/personalData/subscriptions.json', subsSimple='app/personalData/subscriptionsSimplified.json')
	return render_template('subList.html', title='Channel List',
		contents=subList, config=app.config, themeForm=themeForm, channelManager=channelManager)


@app.route('/changeTheme/<theme>')
def changeTheme(theme):
	"""Change the theme for yubtub."""
	app.config['THEME'] = theme
	# prefsObj = prefsManager.parseJSON()
	# prefsObj = prefsManager.modifySetting(prefsObj, 'theme', theme)
	# prefsManager.writeJSON(prefsObj)
	return redirect('/')


@app.route('/user/<username>/popup')
def user_popup(username):
	"""Display popup with more infomation about user username."""
	popupUser = {}
	for user in importLists(subsSimple='app/personalData/subscriptionsSimplified.json'):
		if user.name == username:
			popupUser = user
	popupUser['avatar'] = popupUser['avatar'][:-2] + '128'
	return render_template('user_popup.html', popupUser=popupUser)
